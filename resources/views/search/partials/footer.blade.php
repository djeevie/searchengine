
<div id="searchFooter" class="bg-dark text-white-50 small border-top border-light py-5 px-md-5">
    <span style="clear: both"></span>
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex">
                <ul class="nav ml-0 mr-auto w-100">
                    <li class="nav-item pt-1 h6 font-weight-light m-0">
                        &copy; {{ date('Y') }} {{ config('app.name') }}
                    </li>
                </ul>
                <ul class="nav ml-5 mr-0">
                    <li class="nav-item h6 font-weight-lighter m-0">
                        <small><i>The Open-Source, Decentralized Search Engine</i></small>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</div>
