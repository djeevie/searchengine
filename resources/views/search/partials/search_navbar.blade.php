<div class="sticky-top">
    <div id="searchBarJumbotron"
         class="jumbotron jumbotron-fluid bg-dark py-3 mb-0 shadow-sm">
        <div class="container-fluid">
            <div class="row text-white">
                <div class="d-none d-xl-block col-xl-2"></div>
                <div class="col-12 col-md-4 col-xl-2 my-xl-0 text-right d-flex align-items-center pl-4">
                    <a href="{{ url('/') }}" class="text-white">
                        <span class="fa fa-cloud h3"></span>
                        <span class="h4">SkyGrep <small class="font-weight-light">search</small></span>
                    </a>
                </div>

                <div class="col-12 col-md-4 col-xl-4 mt-xl-0">
                    <div id="searchQueryFormGroup" class="input-group shadow-lg">
                        <input id="searchQueryTextInput" type="text" class="form-control form-control-sm p-3" autofocus
                               tabindex="0">
                        <div class="input-group-append">
                            <button class="btn btm-sm bg-light form-control-sm" type="submit"><span
                                    class="fa fa-search"></span></button>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4 d-flex align-items-center" style="font-size: 90%">
                    <div class="form-group m-0 p-0 small font-weight-bold text-center text-xl-left mt-3 mt-md-0">
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="searchType" value="internet"
                                       checked="checked">Internet
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="searchType" value="images">Images
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="searchType" value="videos">Video's
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row bg-light border-bottom" style="border-color: #718096">
            <div class="col-xl-2"></div>
            <div class="col-12 col-xl-8 text-black-50 small">
                <ul class="nav navbar display-block px-0 py-1">
                    <li class="nav-item">
                        <div class="btn-group">
                            <div class="btn-group">
                                <button type="button" class="btn btn-link btn-sm dropdown-toggle text-black-50" data-toggle="dropdown">
                                    Time Period
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item small" href="#">All</a>
                                    <a class="dropdown-item small" href="#">Past Hour</a>
                                    <a class="dropdown-item small" href="#">Past Day</a>
                                    <a class="dropdown-item small" href="#">Past Month</a>
                                    <a class="dropdown-item small" href="#">Past quarter</a>
                                    <a class="dropdown-item small" href="#">Past year</a>
                                    <hr class="my-2">
                                    <a class="dropdown-item small font-weight-bold" href="#">Cosutom Range...</a>
                                </div>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-link btn-sm dropdown-toggle text-black-50" data-toggle="dropdown">
                                    Language
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item small" href="#">Tablet</a>
                                    <a class="dropdown-item small" href="#">Smartphone</a>
                                </div>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-link btn-sm dropdown-toggle text-light" data-toggle="dropdown">
                                    Region
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item small" href="#">Tablet</a>
                                    <a class="dropdown-item small" href="#">Smartphone</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item d-none d-md-block">25.631 Results for "bla"</li>
                </ul>
            </div>
        </div>
    </div>
</div>

