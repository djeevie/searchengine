@extends('search.default')



@section('title')
    Results for "[searchquery]"
@endsection

@section('content')

    @include('search.partials.search_navbar')

    <div class="container-fluid my-4 my-lg-5 px-4">
        <div id="searchResultsContainer" class="row mt-0 mt-xl-5">
            <div class="d-none d-xl-block col-xl-2"></div>
            <div class="col-12 col-xl-8">

                <div id="searchResults" class="row">
                    <div class="col-12 col-md-8 col-lg-6">
                        @for($l = 1; $l <= 10; $l++)
                            <div class="row mb-4">
                                <div class="col-12">
                                    <a href="#" class="h5">Link number {{ $l }}</a>
                                </div>
                                <div class="col-12">
                                    <span
                                        class="text-success">http://www.{{ \Illuminate\Support\Str::random(6)  }}.com/{{ \Illuminate\Support\Str::random(10) }}</span>
                                </div>
                                <div class="col-12">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 mt-5">
                        <ul class="pagination pagination-sm m-0 d-flex justify-content-left">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </div>

                    <div class="d-none d-xl-block col-xl-2"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
