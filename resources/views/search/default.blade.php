@extends('layouts.base')

@section('app')

    <div id="content">
        @yield('content')
    </div>

    @include('search.partials.footer')
@endsection
