@extends('home.default')

@section('title')
    Home
@endsection

@section('content')
    <div id="pageBg"></div>

    <div id="nightScape"></div>

    {{--    @include('home.partials.search_navbar')--}}

    <div class="container-fluid">
        <div class="row mt-5 mt-lg-5 pt-lg-5">
            <div class="col-12 my-xl-0 text-center">
                <a href="{{ url('/') }}" class="text-white">
                    <span class="fa fa-cloud h1 mr-2"></span>
                    <span class="h1">SkyGrep<small class="font-weight-light ml-2 h5">search</small></span>
                </a>
            </div>
        </div>

        <div class="row text-white mt-2 mt-lg-4">
            <div class="col-12">
                <h1 class="text-center font-weight-bold display-4 text-white-50">Coming Soon</h1>
                <h2 class="text-center mt-5">February 1st, 2021 (Open Bèta)</h2>
                <h4 class="font-weight-light text-center">{{ \Carbon\Carbon::create('01-03-2021')->diffForHumans() }} to go...</h4>
            </div>
        </div>

        <div class="d-lg-block col-4"></div>
    </div>

    <div class="container-fluid my-5">
        <div id="searchResultsContainer" class="d-none row mt-0 mt-xl-5">
            <div class="d-none d-xl-block col-xl-2"></div>
            <div class="col-12 col-xl-8">
                <div class="card">
                    <div id="searchResults" class="card-body">
                        Results...
                    </div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="col-12">
                                <ul class="pagination pagination-sm justify-content-center m-0">
                                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-none d-xl-block col-xl-2"></div>
        </div>
    </div>

    <span class="clear"></span>

    @include('home.partials.footer')
@endsection
