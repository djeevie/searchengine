<nav class="navbar navbar-expand-md bg-dark navbar-dark pl-2 pl-md-3 shadow-sm">
    <a class="navbar-brand mr-auto font-weight-bold ml-2" style="font-size: 1rem;" href="#">
        <span class="fas fa-cloud"></span>
        {{ config('app.name') }}</a>
</nav>
