<!DOCTYPE html>
<html>
<head>
    <title>@yield('title') | SkyGrep</title>
    <meta charset="UTF-8">
    <meta name="application-name" content="SkyGrep">
    <meta name="description" content="Search the internet through billions of indexed results!">
    <meta name="keywords" content="search,search engine,zoekmachine,zoeken,google,bing,yahoo,ddg,duckduckgo,startpage,startpagina,open-source,open source,">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/global.css') }}">
    @yield('head-css')

    <!-- JS libraries -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/global.js') }}"></script>
    @yield('head-scripts')
</head>
<body>
    <div id="app">
        @yield('app')
    </div>

    @yield('body-scripts')
</body>
</html>
