@extends('home.default')

@section('title')
    Home
@endsection

@section('content')
    <div id="pageBg"></div>

    <div id="nightScape"></div>

    {{--    @include('home.partials.search_navbar')--}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-right pt-3">
                <a href="{{ url('register') }}" class="btn btn-link btn-sm font-weight-bold small text-white float-left pt-1 pl-1"><u>Create Account</u></a>
                <a href="#" class="btn btn-primary btn-sm ml-3 float-right">Sign In</a>
            </div>
        </div>
        <form action="{{ url('search') }}" method="GET">
            @csrf

            <div class="row mt-5 mt-lg-5 pt-lg-5">
                <div class="col-12 my-xl-0 text-center">
                    <a href="{{ url('/') }}" class="text-white">
                        <span class="fa fa-cloud h1 mr-2"></span>
                        <span class="h2">SkyGrep<small class="font-weight-light ml-2 h5">search</small></span>
                    </a>
                </div>
            </div>

            <div class="row text-white mt-2 mt-lg-4">
                <div class="d-none d-sm-block col-1 col-md-2 col-lg-3 col-xl-4"></div>

                <div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 mt-3 mt-xl-0">
                    <div id="searchQueryFormGroup" class="input-group shadow-lg">
                        <input id="searchQueryTextInput" type="text" class="form-control" autofocus tabindex="0">
                        <div class="input-group-append">
                            <button type="submit" id="submitQueryButton" value="submit" name="submit"
                                    class="btn bg-light"><span class="fa fa-search"></span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="d-none d-sm-block col-1 col-md-2 col-lg-3 col-xl-4"></div>
            </div>

            <div id="searchBarJumbotronFilterBar" class="row text-white font-weight-bold mt-3">
                <div class="d-lg-block col-4"></div>

                <div class="col-12 col-lg-4">
                    <div class="d-flex form-group m-0 p-0 small font-weight-bold text-xl-left justify-content-center">
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="searchType" value="internet"
                                       checked="checked">Internet
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="searchType" value="images">Images
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="searchType" value="videos">Video's
                            </label>
                        </div>
                    </div>
                </div>

                <div class="d-lg-block col-4"></div>
            </div>
        </form>
    </div>

    <div class="container-fluid my-5">
        <div id="searchResultsContainer" class="d-none row mt-0 mt-xl-5">
            <div class="d-none d-xl-block col-xl-2"></div>
            <div class="col-12 col-xl-8">
                <div class="card">
                    <div id="searchResults" class="card-body">
                        Results...
                    </div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="col-12">
                                <ul class="pagination pagination-sm justify-content-center m-0">
                                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-none d-xl-block col-xl-2"></div>
        </div>
    </div>

    <span class="clear"></span>

    @include('home.partials.footer')
@endsection
