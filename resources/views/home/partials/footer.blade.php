<nav id="indexFooter" class="navbar-dark fixed-bottom text-white-50 shadow-lg small py-4">
    <span style="clear: both"></span>
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex">
                <ul class="nav ml-0">
                    <li class="nav-item font-weight-lighter">
                        &copy; {{ date('Y') }} {{ config('app.name') }}
                    </li>
                </ul>
                <ul class="nav ml-auto">
                    <li class="nav-item font-weight-lighter text-right">
                        The Open-Source, Decentralized Search Engine
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
