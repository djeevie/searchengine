<div id="searchBarJumbotron" class="jumbotron jumbotron-fluid py-3 sticky-top shadow-lg border-bottom border-dark">
    <div class="container-fluid">
        <div class="row text-white">
            <div class="d-none d-xl-block col-xl-2"></div>
            <div class="col-12 col-xl-2 my-xl-0 text-center text-xl-right">
                <a href="{{ url('/') }}" class="text-white">
                    <span class="fa fa-cloud"></span>
                    <span class="h4">SkyGrep <small class="font-weight-light">search</small></span>
                </a>
            </div>

            <div class="d-xl-none d-xl-none col-1"></div>

            <div class="col-12 col-md-10 col-lg-9 col-xl-5 mt-3 mt-xl-0">
                <div id="searchQueryFormGroup" class="input-group shadow-lg">
                    <input id="searchQueryTextInput" type="text" class="form-control" autofocus tabindex="0">
                    <div class="input-group-append">
                        <button class="btn bg-light" type="submit"><span class="fa fa-search"></span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="d-none d-md-block col-md-1 col-lg-1 col-xl-1 mt-1">
                <a href="#" class="text-white"><u>Login</u></a>
            </div>
            <div class="d-lg-none col-2"></div>
        </div>

        <div id="searchBarJumbotronFilterBar" class="row text-white font-weight-bold">

            <div class="container">
                <div class="row pt-3">
                    <div class="d-none d-xl-block col-xl-3"></div>

                    <div class="col-12 col-xl-9 pl-0">
                        <div class="form-group m-0 p-0 small font-weight-bold text-center text-xl-left">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="searchType" value="internet" checked="checked">Internet
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="searchType" value="images">Images
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="searchType" value="videos">Video's
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
