@extends('layouts.base')

@section('app')
    <div id="content">
        @yield('content')
    </div>
@endsection
