<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websites', function (Blueprint $table) {
            $table->id();
            $table->string('url')->index();
            $table->integer("amount_of_webpages")->default(0)->index();
            $table->integer("clicks")->default(0)->index();

            $table->unsignedBigInteger("tld_id")->nullable()->index();
            $table->foreign('tld_id')->references('id')->on('domain_tlds');

            $table->string('alternate_tld')->nullable()->index();
            $table->boolean('secure')->default(false);
            $table->string('robots_txt')->nullable();

            $table->dateTime('last_indexing')->nullable()->index();
            $table->boolean('needs_indexing')->default(false)->index();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('websites');
    }
}
