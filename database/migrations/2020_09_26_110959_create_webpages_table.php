<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebpagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webpages', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->string("page_title")->nullable()->index();
            $table->string('meta_description')->nullable()->index();
            $table->string('meta_keywords')->nullable()->index();
            $table->string('html_content')->nullable()->index();
            $table->string('alternative_description')->index();
            $table->longText('page_dom')->nullable();
            $table->boolean('viewport_settings')->nullable()->index();
            $table->integer('clicks')->default(0)->index();

            $table->unsignedBigInteger('website_id');
            $table->foreign('website_id')->references('id')->on('websites');

            $table->dateTime('last_index')->nullable()->index();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webpages');
    }
}
