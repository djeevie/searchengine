<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorSearchResultClickPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitor_search_result_click_pivot', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('website_id');
            $table->foreign('website_id')->references('id')->on('websites');

            $table->unsignedBigInteger('webpage_id');
            $table->foreign('webpage_id')->references('id')->on('webpages');

            $table->unsignedBigInteger('url_id');
            $table->foreign('url_id')->references('id')->on('html_anchor_elements');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitor_search_result_click_pivots');
    }
}
