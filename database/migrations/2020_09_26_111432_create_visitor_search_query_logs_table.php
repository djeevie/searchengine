<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorSearchQueryLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitor_search_query_logs', function (Blueprint $table) {
            $table->id();
            $table->string('query');
            $table->ipAddress('ip_address');

            $table->unsignedBigInteger('visitor_id')->index();
            $table->foreign('visitor_id')->references('id')->on('visitors_log');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitor_search_query_logs');
    }
}
