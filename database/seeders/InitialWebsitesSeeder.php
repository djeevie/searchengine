<?php

namespace Database\Seeders;

use App\Models\WebContext\Website;
use Illuminate\Database\Seeder;

class InitialWebsitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Website::create([
            'url' => 'https://startpagina.nl',
            'secure' => true,
            'tld_id' => 870
        ]);

        Website::create([
            'url' => 'https://ad.nl',
            'secure' => true,
            'tld_id' => 870
        ]);

        Website::create([
            'url' => 'https://velthove.nl',
            'secure' => true,
            'tld_id' => 870
        ]);

        Website::create([
            'url' => 'https://jensen.nl',
            'secure' => true,
            'tld_id' => 870
        ]);

        Website::create([
            'url' => 'https://nos.nl',
            'secure' => true,
            'tld_id' => 870
        ]);

        Website::create([
            'url' => 'https://nu.nl',
            'secure' => true,
            'tld_id' => 870
        ]);

        Website::create([
            'url' => 'https://rtlnieuws.nl',
            'secure' => true,
            'tld_id' => 870
        ]);
    }
}
