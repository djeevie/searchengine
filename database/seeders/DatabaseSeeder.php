<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TLDSeeder::class);
        $this->call(InitialWebsitesSeeder::class);
        $this->call(DomainsToSkipSeeder::class);
    }
}
