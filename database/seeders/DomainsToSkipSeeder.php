<?php

namespace Database\Seeders;

use App\Models\WebContext\DomainToSkip;
use Illuminate\Database\Seeder;

class DomainsToSkipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DomainToSkip::create([
            'url' => 'googletagmanager.com',
        ]);
    }
}
