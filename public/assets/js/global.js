$(document).ready(function() {
    let vars = {
        'searchFocussed': false,
    };

    let controllers = {
        'changeUiFocus': function() {
            if (vars.searchFocussed) {
                $('#nightScape').removeClass('d-none');
                //$('#pageBg').css('filter', 'blur(8px)')
            }
        }
    };

    let models = {

    };

    let views = {

    }

    function main() {
        //alert(1);

        // Add event listeners.
        $('#searchQueryTextInput').on('keypress', function(e) {
           vars.searchFocussed = true;
           controllers.changeUiFocus();
        });
    }

    main();
});
