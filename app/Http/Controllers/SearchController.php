<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SearchController extends Controller
{
    public function getSearch(SearchRequest $request): View
    {
        return view('search.index');
    }
}
