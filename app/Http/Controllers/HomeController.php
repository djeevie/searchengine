<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function showIndex(): View
    {
        return view('home.index');
    }
}
