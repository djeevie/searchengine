<?php

namespace App\Console\Commands;

use App\Lib\SkyGrep\Crawler\Crawler;
use App\Lib\SkyGrep\Helpers\Helpers;
use App\Lib\SkyGrep\Indexer;
use App\Models\WebContext\DomainToSkip;
use Exception;
use App\Lib\SkyGrep\Scraper\Bin\Scraper;
use App\Models\WebContext\TLD;
use App\Models\WebContext\Website;
use App\Repositories\Cache\Concrete\CacheRepository;
use App\Repositories\Contracts\IWebsiteRepository;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use React\EventLoop\Factory;
use React\Promise\Deferred;
use React\Promise\Promise;
use SplObjectStorage;

/**
 * Class WebCrawlerCommand
 * @package App\Console\Commands
 */
class IndexerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @option --shut-up
     * Tells the application to disable verbose logging.
     *
     * @var string
     */
    protected $signature = 'indexer:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the Indexer to crawl and index the Internet';

    /**
     * @var int
     */
    public static int $websiteAmountPerCycle = 1;

    /**
     * @var int
     */
    public static int $maxAllowedCrawlTasksPerCycle = 2;

    /**
     * @var int
     */
    public static int $maximumDocumentsAllowedToIndexPerWebsite = 1000;

    /**
     * Option --verbose
     *
     * 0 = OMG Just Shut Up!
     * 1 = Show Errors Only.
     * 2 = Show Errors and Warnings.
     * 3 = Show Errors, Warnings and the Website being handled.
     * 4 = Show Errors, Warnings, the Website being handled and Current Activity.
     * 5 = Show Errors, Warnings, the Website being handled and Current Activity. Also show failed document fetches.
     *     Keeps talking without end, like your mom.
     *
     * @var int
     */
    public static int $verboseLevel = 5;

    /**
     * @var IWebsiteRepository
     */
    private IWebsiteRepository $websiteRepo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->websiteRepo = app()->make(IWebsiteRepository::class);

        try {
            $requiredClasses = [
                Factory::class,
                Deferred::class,
                Promise::class,
                Client::class
            ];

            foreach ($requiredClasses as $class) {
                if (!class_exists($class)) {
                    throw new Exception('Please install ' . $class . '.');
                }
            }
        } catch (Exception $e) {
            $this->error($e->getMessage());

            die();
        }
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo '   _____ _           _____                ' . PHP_EOL;
        echo '  / ____| |         / ____|               ' . PHP_EOL;
        echo ' | (___ | | ___   _| |  __ _ __ ___ _ __  ' . PHP_EOL;
        echo '  \___ \| |/ | | | | | |_ | \'__/ _ | \'_ \ ' . PHP_EOL;
        echo '  ____) |   <| |_| | |__| | | |  __| |_) |' . PHP_EOL;
        echo ' |_____/|_|\_\\__, |\_____|_|  \___| .__/ ' . PHP_EOL;
        echo '               __/ |               | |    ' . PHP_EOL;
        echo '              |___/                |_|    ' . PHP_EOL;

        echo PHP_EOL;

        $this->line(PHP_EOL . config('app.name') . ' Search Engine v0.1 (codename ISAEUS)' . PHP_EOL . PHP_EOL .
            '(C) ' . date('Y') . ' Davey Velthove. All rights reserved.' . PHP_EOL .
            'Please read and respect the LICENSE.txt file.' . PHP_EOL);

        echo PHP_EOL;
        $this->line('- Loading...');

        $this->bootstrap();

        $applicationStartTime = Carbon::now();
        $applicationFinishedTime = null;

        // Start the application!
        $this->main();

        // Application done!
        $applicationFinishedTime = Carbon::now();

        $this->alert('Done!');
        $this->info('This operation took ' . $applicationStartTime->diffInDays($applicationFinishedTime) .
            ' days, ' . $applicationStartTime->diffInHours($applicationFinishedTime) . ' hours and ' .
            $applicationStartTime->diffInMinutes($applicationFinishedTime) . ' minutes.');

        $indexer = Indexer::getInstance();

        $this->line((string)$indexer->getIndexedWebsitesCounter() . ' websites have been indexed.');
        $this->line((string)$indexer->getIndexedDocumentsCounter() . ' documents have been indexed.');

        return 0;
    }

    /**
     *
     */
    private function main()
    {
        $this->info('- Running!' . PHP_EOL);

        $indexer = Indexer::getInstance();

        // Fetch websites from the database.
        Website::chunk(3, function (Collection $websites) use (&$indexer) {
            foreach ($websites as $website) {
                // Has the website been indexed recently?
                if (!is_null($website->last_indexing)) {
                    if (Carbon::make($website->last_indexing)->diffInDays() < 3) {
                        continue;
                    }
                }

                $indexer->attachWebsite($website);
            }

            if ($indexer->checkAndUpdateOperatingStatus() == false) {
                $indexer->run();
            }
        });
    }

    /**
     * Bootstrap before running the command. Takes care of dependencies.
     *
     * @return void
     */
    private function bootstrap(): void
    {
        $tlds = TLD::all();
        CacheRepository::putByString('tlds', $tlds);

        $domainsToSkip = DomainToSkip::all();
        CacheRepository::putByString('domains_to_skip', $domainsToSkip);
    }
}
