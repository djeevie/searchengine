<?php

namespace App\Events;

use App\Lib\SkyGrep\Indexer;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class IndexingWebsiteCompletedEvent
 * @package App\Events
 */
class IndexingWebsiteCompletedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Indexer
     */
    public Indexer $indexerInstance;

    /**
     * @var object
     */
    public object $website;

    /**
     * @var int
     */
    public int $amountOfDocumentsIndexed;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(object $website)
    {dd('HALLO');
        $this->indexerInstance = Indexer::getInstance();
        $this->website = $website;
        $this->amountOfDocumentsIndexed = $amountOfDocumentsIndexed;
    }
}
