<?php

namespace App\Events;

use App\Models\WebContext\Webpage;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class IndexingDocumentCompletedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Webpage $webpage;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Webpage $webpage)
    {
        $this->webpage = $webpage;
    }
}
