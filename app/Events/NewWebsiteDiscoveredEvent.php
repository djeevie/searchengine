<?php

namespace App\Events;

use App\Models\WebContext\Webpage;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NewWebsiteDiscoveredEvent
 * @package App\Events
 */
class NewWebsiteDiscoveredEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public string $url;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $url)
    {
        echo 'NEW WEBSITE DISCOVERED: ' . $url . PHP_EOL;
        $this->url = $url;
    }
}
