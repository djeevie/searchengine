<?php

namespace App\Repositories\Eloquent;

use Exception;
use App\Models\User;
use App\Models\WebContext\Website;
use App\Repositories\Contracts\IWebsiteRepository;
use App\Repositories\RepositoryAbstract;
use Illuminate\Support\Collection;
use React\Promise\Deferred;
use React\Promise\Promise;

/**
 * Class EloquentWebsiteRepository
 * @package App\Repositories\Eloquent
 */
class EloquentWebsiteRepository extends RepositoryAbstract implements IWebsiteRepository
{
    /**
     * @return string
     */
    public function entity(): string
    {
        return Website::class;
    }

    /**
     * @param int $chunkSize
     * @return Promise
     */
    public function getWebsitesAsync(int $chunkSize = 0): Promise
    {
        $deferred = new Deferred();

        try {
            if ($chunkSize > 0) {
                $resultSet = Website::chunk($chunkSize, function (Collection $websites) {
                    return $websites;
                });
            } else {
                $resultSet = Website::all();
            }

            $deferred->resolve($resultSet);
        } catch (Exception $e) {
            $deferred->reject($e);
        }

        return $deferred->promise();
    }
}
