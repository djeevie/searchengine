<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\Contracts\IUserRepository;
use App\Repositories\RepositoryAbstract;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class EloquentUserRepositoryRepository
 * @package App\Repositories\Eloquent
 */
class EloquentUserRepository extends RepositoryAbstract implements IUserRepository
{
    /**
     * @return string
     */
    public function entity(): string
    {
        return User::class;
    }

    /**
     * @return Collection
     */
    public function getAllUsersFromCache(): Collection
    {
        $users = null;

        if (!Cache::has('users')) {
            $users = $this->all();
            Cache::put('users', $users, 3600);
        } else {
            $users = Cache::get('users');
        }

        return $users;
    }

    /**
     * @return Collection
     */
    public function getLoggedInUsers(): Collection
    {
        $fromDateTime = Carbon::now()->subMinutes(5)->toDateTime();

        $users = $this->entity->where('last_seen', '>', $fromDateTime)->get();

        return $users;
    }

    /**
     * @return Collection
     */
    public function getLoggedInUsersFromCache(): Collection
    {
        $users = null;

        if (!Cache::has('users_active')) {
            $users = $this->getLoggedInUsers();
            Cache::put('users_active', $users, 3600);
        } else {
            $users = Cache::get('users_active');
        }

        return $users;
    }
}
