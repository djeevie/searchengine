<?php

namespace App\Repositories\Eloquent;

use App\Models\WebContext\Webpage;
use App\Repositories\Contracts\IWebpageRepository;
use Exception;
use App\Models\User;
use App\Models\WebContext\Website;
use App\Repositories\Contracts\IWebsiteRepository;
use App\Repositories\RepositoryAbstract;
use Illuminate\Support\Collection;
use React\Promise\Deferred;
use React\Promise\Promise;

/**
 * Class EloquentWebsiteRepository
 * @package App\Repositories\Eloquent
 */
class EloquentWebpageRepository extends RepositoryAbstract implements IWebpageRepository
{
    /**
     * @return string
     */
    public function entity(): string
    {
        return Webpage::class;
    }
}
