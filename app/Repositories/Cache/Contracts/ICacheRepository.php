<?php

namespace App\Repositories\Cache\Contracts;

/**
 * Interface ICacheRepository
 * @package App\Repositories\Cache\Contracts
 */
interface ICacheRepository
{
    /**
     * @param string $name
     * @return mixed
     */
    public static function getByString(string $name);

    /**
     * @param string $name
     * @param mixed $value
     * @return mixed
     */
    public static function putByString(string $name, $value, int $ttl);

    /**
     * @param string $name
     * @return mixed
     */
    public static function deleteByString(string $name);
}
