<?php

namespace App\Repositories\Cache\Concrete;

use App\Repositories\Cache\Contracts\ICacheRepository;
use Illuminate\Support\Facades\Cache;

class CacheRepository implements ICacheRepository
{
    /**
     * @param string $name
     * @return mixed
     */
    public static function getByString(string $name)
    {
        if (Cache::has($name)) {
            return Cache::get($name);
        } else {
            return null;
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public static function putByString(string $name, $value, $ttl = 0)
    {
        if (Cache::has($name)) {
            Cache::forget($name);
        }

        if ($ttl == 0) {
            Cache::forever($name, $value);
        } else {
            Cache::put($name, $value, $ttl);
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public static function deleteByString(string $name)
    {
        Cache::forget($name);
    }

}
