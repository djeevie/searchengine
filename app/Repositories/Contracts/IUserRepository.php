<?php

namespace App\Repositories\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface IUserRepository
 * @package App\Repositories\Contracts
 */
interface IUserRepository
{
    /**
     * @return Collection
     */
    public function getAllUsersFromCache(): Collection;

    /**
     * @return Collection
     */
    public function getLoggedInUsers(): Collection;

    /**
     * @return Collection
     */
    public function getLoggedInUsersFromCache(): Collection;
}
