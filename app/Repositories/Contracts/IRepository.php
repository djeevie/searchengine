<?php

namespace App\Repositories\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface IRepository
 * @package App\Repositories\Contracts
 */
interface IRepository
{
    /**
     * @return Collection|null
     */
    public function all(): ?Collection;

    /**
     * @param int $id
     * @return object|null
     */
    public function find(int $id): ?object;

    /**
     * @param string $column
     * @param string $value
     * @return Collection
     */
    public function findWhere(string $column, string $value): Collection;

    /**
     * @param string $column
     * @param string $value
     * @return object|null
     */
    public function findWhereFirst(string $column, string $value): ?object;

    /**
     * @param int $shownObjectsPerPage
     * @return mixed
     */
    public function paginate(int $shownObjectsPerPage = 10);

    /**
     * @param array $properties
     * @return mixed
     */
    public function create(array $properties);

    /**
     * @param int $id
     * @param array $properties
     * @return bool
     */
    public function update(int $id, array $properties): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}
