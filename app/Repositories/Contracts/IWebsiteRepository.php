<?php

namespace App\Repositories\Contracts;

use React\Promise\Promise;

interface IWebsiteRepository
{
    public function getWebsitesAsync(int $chunkSize = 0): Promise;
}
