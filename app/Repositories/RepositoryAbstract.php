<?php

namespace App\Repositories;

use Exception;
use App\Repositories\Contracts\IRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class RepositoryAbstract
 * @package App\Repositories
 */
abstract class RepositoryAbstract implements IRepository
{
    /**
     * @var object
     */
    protected object $entity;

    /**
     * RepositoryAbstract constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->entity = $this->resolveEntity();
    }

    /**
     * @return string
     */
    abstract public function entity(): string;

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->entity->all();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id): ?object
    {
        // TODO: Implement find() method.
    }

    /**
     * @param string $column
     * @param string $value
     * @return Collection
     */
    public function findWhere(string $column, string $value): Collection
    {
        return $this->entity->where($column, '=', $value)->get();
    }

    /**
     * @param string $column
     * @param string $value
     * @return object|null
     */
    public function findWhereFirst(string $column, string $value): ?object
    {
        return $this->entity->where($column, '=', $value)->first();
    }

    /**
     * @param int $shownObjectsPerPage
     */
    public function paginate(int $shownObjectsPerPage = 10)
    {
        return $this->entity->paginate($shownObjectsPerPage);
    }

    /**
     * @param array $properties
     * @return mixed
     */
    public function create(array $properties)
    {dd($properties);
        return $this->entity->create($properties);
    }

    /**
     * @param int $id
     * @param array $properties
     * @return bool
     */
    public function update(int $id, array $properties): bool
    {
        $object = $this->entity->find($id);

        return $object->update($properties);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->entity->delete($id);
    }

    /**
     * @return object
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function resolveEntity(): object
    {
        if (!method_exists($this, 'entity')) {
            throw new Exception('Entity not found!');
        }

        return app()->make($this->entity());
    }
}
