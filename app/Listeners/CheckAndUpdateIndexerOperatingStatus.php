<?php

namespace App\Listeners;

use App\Lib\SkyGrep\Indexer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CheckAndUpdateIndexerOperatingStatus
{
    private Indexer $indexer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->indexer = Indexer::getInstance();
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {dd('HELLO');
        $this->indexer->checkAndUpdateOperatingStatus();
    }
}
