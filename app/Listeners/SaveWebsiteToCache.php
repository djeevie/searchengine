<?php

namespace App\Listeners;

use App\Repositories\Cache\Concrete\CacheRepository;
use App\Repositories\Contracts\IWebsiteRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SaveWebsiteToCache
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        CacheRepository::putByString($event->website->url, $event->website->url);
    }
}
