<?php

namespace App\Listeners;

use App\Events\NewWebsiteDiscoveredEvent;
use App\Lib\SkyGrep\Networking\Bin\HttpContext\Get\HttpGet;
use App\Models\WebContext\TLD;
use App\Repositories\Cache\Concrete\CacheRepository;
use App\Repositories\Contracts\IWebsiteRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use React\Promise\Promise;

/**
 * Class AddWebsiteToDatabase
 * @package App\Listeners
 */
class AddWebsiteToDatabase
{
    /**
     * @var IWebsiteRepository
     */
    private IWebsiteRepository $websiteRepo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->websiteRepo = app()->make(IWebsiteRepository::class);
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(NewWebsiteDiscoveredEvent $event)
    {
        $httpClient = new HttpGet();
        $fetchDocumentTask = $httpClient->fetchDocumentAsync($event->url);

        if (!$fetchDocumentTask instanceof Promise) {
            echo '[ERROR] Could not fetch website ' . $event->url . 'in AddWebsiteToDatabase.php.' . PHP_EOL;
            return;
        }

        $fetchDocumentTask->then(function ($asyncResult) use (&$event) {
            $tlds = CacheRepository::getByString('tlds');
            $websiteTld = null;
            $securedWebsite = false;

            foreach ($tlds as $tldObject) {
                if (is_int(strpos($event->url, $tldObject->TLD))) {
                    $websiteTld = $tldObject;
                }
            }

            if (is_null($websiteTld)) {
                // An URL can contain multiple dots.
                $dotPositions = [

                ];

                $searchingForTld = true;
                $currentAttempts = 0;

                do {
                    $position = strpos('.', $event->url);

                    if (is_int($position)) {
                        $dotPositions[] = $position;
                    } else {
                        break;
                    }

                    $currentAttempts++;
                    if ($currentAttempts > 10) {
                        $searchingForTld = false;
                    }
                } while ($searchingForTld);

                $websiteTld = str_split($event->url, array_pop($dotPositions) - 1);
            }

            // Is the website secure?
            if (strpos('https', $event->url)) {
                $securedWebsite = true;
            }

            $this->websiteRepo->create([
                'url' => $event->url,
                'tld_id' => $websiteTld instanceof TLD ? $websiteTld->id : null,
                'alternate_tld' => $websiteTld instanceof TLD ? $websiteTld : null,
                'secure' => $securedWebsite,
            ]);
        });
    }
}
