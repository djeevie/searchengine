<?php

namespace App\Listeners;

use App\Lib\SkyGrep\Indexer;
use App\Models\WebContext\Website;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateIndexerDocCounters
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        $event->indexerInstance->updateIndexedWebsitesCounter(1);
        $event->indexerInstance->updateIndexedDocumentsCounter($event->indexedDocumentsCounter);
    }
}
