<?php

namespace App\Listeners;

use App\Repositories\Contracts\IWebsiteRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateWebsiteObjectInDb
{
    private IWebsiteRepository $websiteRepo;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->websiteRepo = app()->make(IWebsiteRepository::class);
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $event->website->last_indexing = now();
        $event->website->needs_indexing = false;

        // Do not use the repository to update the object to prevent I/O.
        $event->website->update($event->website->id, [
            'last_indexing' => now(),
            'needs_indexing' => false,
        ]);
    }
}
