<?php

namespace App\Lib\SkyGrep\Helpers;

/**
 * Class Helpers
 * @package App\Lib\SkyGrep\Helpers
 */
class Helpers
{
    /**
     * This function takes any given URL and parses it to something useful.
     * Example:
     * https://example.com/index.php?show_amount=15 >> becomes >>  https://example.nl
     *
     * @param string $url
     * @return string
     */
    public static function parseUrlToCleanDomainUrl(string $url): string
    {
        $protocolEndPosition = strpos($url, '://');

        $protocol = substr($url, 0, $protocolEndPosition);
        $domainTemp = substr($url, $protocolEndPosition + 3, strlen($url));

        $firstUriSlashOccurrence = strpos($domainTemp, '/');

        $domain = substr($domainTemp, 0, $firstUriSlashOccurrence);

        return $protocol . '://' . $domain;
    }
}
