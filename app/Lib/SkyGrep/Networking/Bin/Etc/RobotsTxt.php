<?php

namespace App\Lib\SkyGrep\Networking\Bin\Etc;

use Exception;
use Illuminate\Support\Collection;
use SplObjectStorage;

/**
 * Class RobotsTxt
 * @package App\Lib\WebCrawler\Bin\Http
 */
final class RobotsTxt
{
    /**
     * @var array
     */
    public array $allowedDirectories;

    /**
     * @var array
     */
    public array $disallowedDirectories;

    /**
     * @var array
     */
    public array $xmlSiteMaps;

    /**
     * RobotsTxt constructor.
     * @param array $disallowedDirectories
     */
    public function __construct(array $allowedDirectories = [], array $disallowedDirectories = [])
    {
        $this->allowedDirectories = [];
        $this->disallowedDirectories = [];
        $this->xmlSiteMaps = [];

        foreach ($allowedDirectories as $directory) {
            $this->allowedDirectories[] = $directory;
        }

        foreach ($disallowedDirectories as $directory) {
            $this->disallowedDirectories[] = $directory;
        }
    }
}
