<?php


namespace App\Lib\SkyGrep\Networking\Bin\HttpContext;

use GuzzleHttp\Client;
use React\Promise\Deferred;

/**
 * Class HttpBase
 * @package App\Lib\WebCrawler\Bin\Http
 */
abstract class HttpBase
{
    /**
     * @var string
     */
    public string $url;

    /**
     * Static Cookie Jar for cURL.
     * @var string
     */
    public static string $cookieJar;

    /**
     * @var Client
     */
    public Client $httpClient;

    /**
     * @var Deferred
     */
    public Deferred $deferred;

    /**
     * Contains a list with random user-agent strings. Is used primarily to keep SkyGrep in 'stealth scraping-mode'. ;)
     *
     * @var array
     */
    protected array $userAgentStrings;

    /**
     * HttpBase constructor.
     */
    public function __construct()
    {
        $this->deferred = new Deferred();

        self::$cookieJar = '';

        $this->userAgentStrings = [
            // Windows
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0',
            'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36',
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.52',
            'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063',

            // Apple
            'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36 OPR/47.0.2631.55',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:56.0) Gecko/20100101 Firefox/56.0',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/604.3.5 (KHTML, like Gecko) Version/11.0.1 Safari/604.3.5',

            // Linux
            'Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0',

            // Android
            'Mozilla/5.0 (Linux; Android 9.0; MI 8 SE) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.119 Mobile Safari/537.36',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 13_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML%2C like Gecko) Mobile/15E148',

            // Robots
            'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
            'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)',
            'Mozilla/5.0 (compatible; bingbot/2.0 +http://www.bing.com/bingbot.htm)',
            'DuckDuckBot/1.0; (+http://duckduckgo.com/duckduckbot.html)',
            'Googlebot/2.1 (+http://www.googlebot.com/bot.html)',
            'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
        ];
    }

    /**
     * @param string $url
     * @param array $options
     * @return false|resource
     */
    public function setCurlOptionsAndGetInstance(string $url, array $options = [])
    {
        $curlInstance = curl_init($url);
        curl_setopt($curlInstance, CURLOPT_COOKIE, self::$cookieJar);
        curl_setopt($curlInstance, CURLOPT_HEADER, $this->getRandomUserAgentString());
        curl_setopt($curlInstance, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($curlInstance, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlInstance, CURLOPT_FOLLOWLOCATION, true);

        return $curlInstance;
    }

    /**
     * Get a random user-agent string and return it.
     *
     * @return string
     */
    protected function getRandomUserAgentString(): string
    {
        $rand = random_int(0, count($this->userAgentStrings) - 1);

        return $this->userAgentStrings[$rand];
    }
}
