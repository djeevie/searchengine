<?php

namespace App\Lib\SkyGrep\Networking\Bin\HttpContext\Get;

use App\Lib\SkyGrep\Networking\Bin\HttpContext\HttpBase;
use Exception;
use DOMDocument;
use React\Promise\Promise;

/**
 * Class HttpGet
 * @package App\Lib\WebCrawler\Bin\Http
 */
final class HttpGet extends HttpBase
{
    /**
     * HttpGet constructor.
     * @param string $url
     */
    public function __construct(string $url = null)
    {
        parent::__construct();

        if (!is_null($url)) {
            $this->url = $url;
        }
    }

    /**
     * @return Promise
     */
    public function fetchDocumentAsync(string $url): Promise
    {
        $this->url = $url;

        try {
            $this->deferred->resolve($this->fetchDocument($url));

            return $this->deferred->promise();
        } catch (Exception $e) {
            $this->deferred->reject();
            return $this->deferred->promise();
        }
    }

    /**
     * @return DOMDocument|null
     */
    public function fetchDocument(string $url): ?DOMDocument
    {
        try {
            if (!empty($this->url)) {
                $url = $this->url;
            }

            // Prepare cURL; the URL to fetch gets retrieved from the base class.
            $curlInstance = $this->setCurlOptionsAndGetInstance($url);

            $httpFetchRoughResult = curl_exec($curlInstance);
            curl_close($curlInstance);

            libxml_use_internal_errors(true);
            $pageDOM = new DOMDocument();
            $pageDOM->loadHTML($httpFetchRoughResult);
            libxml_clear_errors();

            return $pageDOM;
        } catch (Exception $e) {
            echo '[ERROR] ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;

            return null;
        }
    }
}
