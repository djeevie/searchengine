<?php

namespace App\Lib\SkyGrep\Scraper\Bin\DNS;

use Exception;
use React\Dns\Config\Config;
use React\Dns\Resolver\Factory;
use React\Promise\Promise;
use SplObjectStorage;
use App\Models\WebContext\Domain;

/**
 * Class FetchDNS
 * @package App\Lib\WebCrawler\Bin\DNS\FetchDNS
 */
final class FetchDNS
{
    /**
     * @var $this
     */
    private static self $instance;

    /**
     * @var SplObjectStorage
     */
    private SplObjectStorage $domains;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var mixed|string
     */
    private $server;

    /**
     * @var
     */
    private $dns;

    /**
     * FetchDNS constructor.
     */
    private function __construct()
    {
        $this->domains = new SplObjectStorage();
        $this->config = Config::loadSystemConfigBlocking();
        $this->server = $this->config->nameservers ? reset($this->config->nameservers) : '8.8.8.8';
    }

    /**
     * @return static
     */
    public static function getInstance(): self
    {
        if (empty(self::$instance)) {
            self::$instance = new FetchDNS;
            self::$instance->getInstance()->run();
        }

        return self::$instance;
    }

    /**
     * @param Domain $domain
     */
    public function attachDomain(Domain $domain)
    {
        $this->domains->attach($domain);
    }

    /**
     * @param $loop
     * @return Promise
     */
    private function resolveDomain (&$loop)
    {
        $this->dns = (new Factory())->createCached($this->server, $loop);

        foreach ($this->domains as $domain) {
            try {
                $resolvedIp = '';

                $this->dns->resolve($domain)->then(function($ip) use (&$resolvedIp) {
                    $resolvedIp = $ip;
                });

                return $resolvedIp;
            } catch (Exception $e) {
                continue;
            }
        }
    }
}
