<?php

namespace App\Lib\SkyGrep\Crawler;

use DOMDocument;
use React\Promise\Deferred;
use React\Promise\Promise;

/**
 * Class Prism
 * @package App\Lib\SkyGrep\Prism
 */
class Crawler
{
    /**
     * @var $this
     */
    private static Crawler $instance;

    /**
     * Prism constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return Crawler
     */
    public static function getInstance(): Crawler
    {
        if (!static::$instance instanceof self) {
            static::$instance = new Crawler;
        }

        return self::$instance;
    }

    /**
     * Takes the DOMDocument and crawls it
     * @return void
     */
    public static function crawlDocumentAsync(DOMDocument $document): Promise
    {

    }
}
