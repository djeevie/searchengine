<?php

namespace App\Lib\SkyGrep\Prism;

/**
 * Class Prism
 * @package App\Lib\SkyGrep\Prism
 */
class Prism
{
    /**
     * @var $this
     */
    private static Prism $instance;

    /**
     * Prism constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return Prism
     */
    public static function getInstance(): Prism
    {
        if (!static::$instance instanceof self) {
            static::$instance = new Prism;
        }

        return self::$instance;
    }

    /**
     * @return void
     */
    public function main(): void
    {

    }
}
