<?php

namespace App\Lib\SkyGrep;

use App\Console\Commands\IndexerCommand;
use App\Events\IndexingDocumentCompletedEvent;
use App\Events\NewWebpageDiscoveredEvent;
use App\Events\NewWebsiteDiscoveredEvent;
use App\Events\ScrapingWebpageCompletedEvent;
use App\Lib\SkyGrep\Helpers\Helpers;
use App\Lib\SkyGrep\Networking\Bin\Etc\RobotsTxt;
use App\Events\IndexingWebsiteCompletedEvent;
use App\Lib\SkyGrep\Networking\Bin\HttpContext\Get\HttpGet;
use App\Lib\SkyGrep\Scraper\Bin\Elements\AnchorElement;
use App\Lib\SkyGrep\Scraper\Bin\Elements\MetaTags\MetaTags;
use App\Lib\SkyGrep\Scraper\Bin\Elements\Shapes\WebpageShape;
use App\Models\WebContext\Webpage;
use App\Models\WebContext\Website;
use App\Repositories\Cache\Concrete\CacheRepository;
use App\Repositories\Contracts\IWebpageRepository;
use App\Repositories\Contracts\IWebsiteRepository;
use Carbon\Carbon;
use DOMDocument;
use DOMElement;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Event;
use React\Promise\Deferred;
use React\Promise\Promise;
use SplObjectStorage;

/**
 * Class Indexer
 * @package App\Lib\SkyGrep
 */
class Indexer
{
    /**
     * @var Indexer
     */
    private static Indexer $instance;

    /**
     * @var IWebsiteRepository
     */
    private IWebsiteRepository $websiteRepo;

    /**
     * @var IWebpageRepository
     */
    private IWebpageRepository $webpageRepo;

    /**
     * @var SplObjectStorage
     */
    private SplObjectStorage $websiteQueue;

    /**
     * @var SplObjectStorage
     */
    private SplObjectStorage $crawlingTasks;

    /**
     * @var SplObjectStorage
     */
    private SplObjectStorage $handleDocumentTasks;

    /**
     * Contains an array with the URL's of documents that have been indexed already.
     * Reflects the documents which have to be skipped by the Indexer.
     *
     * @var Collection
     */
    private Collection $indexedUrls;

    /**
     * The amount of websites indexed.
     *
     * @var int
     */
    private int $indexedWebsitesCounter = 0;

    /**
     * The amount of documents indexed.
     *
     * @var int
     */
    private int $indexedDocumentsCounter = 0;

    /**
     * The current working status of the instance.
     *
     * @var bool
     */
    private bool $isOperating = false;

    /**
     * Crawler constructor.
     * @param IWebsiteRepository $websiteRepo
     */
    private function __construct()
    {
        $this->websiteRepo = app()->make(IWebsiteRepository::class);
        $this->webpageRepo = app()->make(IWebpageRepository::class);
        $this->websiteQueue = new SplObjectStorage();
        $this->crawlingTasks = new SplObjectStorage();
        $this->scrapingTasks = new SplObjectStorage();
        $this->indexedUrls = new Collection();
    }

    /**
     * @return Indexer
     */
    public static function getInstance(): Indexer
    {
        if (empty(self::$instance)) {
            self::$instance = new Indexer;
        }

        return self::$instance;
    }

    /**
     * @return int
     */
    public function getIndexedWebsitesCounter(): int
    {
        return $this->indexedWebsitesCounter;
    }

    /**
     * @return int
     */
    public function getIndexedDocumentsCounter(): int
    {
        return $this->indexedDocumentsCounter;
    }

    /**
     * Run the Indexer.
     *
     * @return void
     */
    public function run(): void
    {
        $this->isOperating = true;

        foreach ($this->websiteQueue as $website) {
            try {
                $deferredHandleWebsiteTask = new Deferred();
                $deferredHandleWebsiteTask->resolve($this->handleWebsite($website));
                $promise = $deferredHandleWebsiteTask->promise();

                $promise->then(function (int $asyncResult) use (&$website) {
                    // Indexing the website had been completed, fire event.
                    event(new IndexingWebsiteCompletedEvent($website));
                })->otherwise(function ($asyncResult) use (&$website) {
                    throw new Exception('Could not handle website ' . $website->url);
                });

                $this->crawlingTasks->attach($deferredHandleWebsiteTask);
            } catch (Exception $e) {
                if (IndexerCommand::$verboseLevel > 0) {
                    echo '[ERROR] ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;
                }
            }
        }
    }

    /**
     * @param Collection $websites
     * @return void
     */
    public function attachWebsite(Website $website): void
    {
        self::$instance->websiteQueue->attach($website);

        return;
    }

    /**
     * @param int $amount
     */
    public function updateIndexedWebsitesCounter(int $amount): void
    {
        $this->indexedWebsitesCounter = $this->indexedWebsitesCounter + $amount;
    }

    /**
     * @param int $amount
     */
    public function updateIndexedDocumentsCounter(int $amount): void
    {
        $this->indexedDocumentsCounter = $this->indexedDocumentsCounter + $amount;
    }

    /**
     * If there are any running indexing tasks running, it handles the status and returns it.
     *
     * @return bool
     */
    public function checkAndUpdateOperatingStatus(): bool
    {
        if ($this->crawlingTasks->count() > 0 || $this->scrapingTasks->count() > 0) {
            $this->isOperating = true;
        } else {
            $this->isOperating = false;
        }

        return $this->isOperating;
    }

    /**
     * TL;TR: This function is responsible for crawling and scraping the specified website.
     *
     * It firstly fetches the robots.txt file. If it exists, a new Robots-class gets instantiated.
     * Then it will fetch the 'default document' from the server. This might either consist of a directory
     * index or webpage.
     *
     * The crawling and indexing of the website starts from here.
     * Every discovered Document gets passed to $this->handleDocumentAsync().
     *
     * Please notice this library uses random User-agent strings, because SkyGrep is like a buddha
     * waving through the seas of the Internet, always remembering what he saw that day. Anonymously.
     *
     * @param object $website
     * @param array $optionalArgs
     *              'fetchRobotsTxtFile': Fetch robots.txt or not (default: true).
     *
     *              'ignoreRobotsFile': Forced scraping of the whole website. Ignore the webmaster's preferences.
     *                                  (default: false)
     *
     *              'maxCrawlLevel': Specify the maximal allowed page level depth from the Default Document.
     *                              Example:
     *                              0 = Scrape complete website (default)
     *                              1 = Only crawl and index the Default Document.
     *                              2 = Two levels deep. Scrape the Default Document and all referenced documents on that page.
     *                              3 = Crawl and index three levels deep.
     *                              4 = Etc...
     *
     *              'pageLimit':    If set higher than 0 (default), the amount of pages scraped and indexed gets limited.
     *                              Use with caution; only use this parameter for extremely large websites.
     *
     * @return int
     */
    public function handleWebsite(Website $website, array $optionalArgs = []): int
    {
        if (IndexerCommand::$verboseLevel >= 3) {
            echo '[WEBSITE]: ' . $website->url . PHP_EOL;
        }

        // URL's found in robots.txt
        $xmlUrlsToScrape = new Collection();

        // Default settings.
        $fetchRobotsTxtFile = true;
        $ignoreRobotsFile = false;
        $maxCrawlLevel = 0;
        $pageLimit = 0;

        // Override settings if passed to this function.
        if (count($optionalArgs) > 0) {
            foreach ($optionalArgs as $arg) {
                switch ($arg) {
                    case 'fetchRobotsTxtFile':
                        $fetchRobotsTxtFile = $arg;
                        break;
                    case 'ignoreRobotsFile':
                        $ignoreRobotsFile = $arg;
                        break;
                    case 'maxCrawlLevel':
                        $maxPageLevel = $arg;
                        break;
                    case 'pageLimit':
                        $pageLimit = $arg;
                        break;
                    default:
                        break;
                }
            }
        }

        /*
         * Fetch robots.txt
         * */

        $robotsTxt = new RobotsTxt();
        $deferredFetchRobotsTxtFile = new Deferred();

        try {
            if (IndexerCommand::$verboseLevel >= 4) {
                echo '[HTTP GET]: ' . $website->url . '/robots.txt' . PHP_EOL;
            }
            // Firstly, try fetching the robots.txt file before continuing.
            $fetchedRobotsFile = file_get_contents($website->url . '/robots.txt');
        } catch (Exception $e) {
            if (IndexerCommand::$verboseLevel >= 5) {
                echo 'Couldn\'t fetch robots.txt for ' . $website->url . PHP_EOL;
            }

            $deferredFetchRobotsTxtFile->reject($e);
        }

        if ($fetchedRobotsFile != null) {
            $deferredFetchRobotsTxtFile->resolve($fetchedRobotsFile);
        } else {
            $deferredFetchRobotsTxtFile->reject('No robots.txt has been found on the server.');
        }

        // Parse the fetched Robots.txt-file.
        ($deferredFetchRobotsTxtFile->promise())->then(function ($fetchedRobotsFile) use (&$robotsTxt) {
            try {
                // Search the file for lines with relevant information.
                if (isset($fetchedRobotsFile)) {
                    // Method for filtering URL's.
                    $directoryFilterFunc = function ($line): string {
                        $firstSlashOccurrence = strpos($line, '/');
                        $directory = filter_var(substr($line, $firstSlashOccurrence, strlen($line)),
                            FILTER_SANITIZE_STRING
                        );

                        $directory = str_replace('*', '', $directory);

                        return $directory;
                    };

                    // Anonymous method for filtering XML URL's.
                    $xmlFilerFunc = function ($line): string {
                        $httpPosition = strpos($line, 'http');
                        return substr($line, $httpPosition, strlen($line));
                    };

                    // Handle the robots.txt file line by line.
                    foreach (preg_split('@\n@', $fetchedRobotsFile) as $line) {
                        // Is this the User-agent string? Skip.
                        if (strpos($line, 'agent')) {
                            continue;
                        }

                        if (str_contains($line, 'Disallow') && is_int(strpos($line, '/'))) {
                            $robotsTxt->disallowedDirectories[] = $directoryFilterFunc($line);
                        } elseif (str_contains($line, 'Allow') && is_int(strpos($line, '/'))) {
                            $robotsTxt->allowedDirectories[] = $directoryFilterFunc($line);
                        } elseif (str_contains($line, 'xml')) {
                            $robotsTxt->xmlSiteMaps[] = $xmlFilerFunc($line);
                        }
                    }
                }
            } catch (Exception $e) {
                if (IndexerCommand::$verboseLevel > 0) {
                    echo '[ERROR] ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;
                }
            }
        }, function ($result) use (&$robotsTxt) {
            $robotsTxt = null;
            if (IndexerCommand::$verboseLevel >= 2) {
                echo '[WARNING] Could not handle the robots.txt file (Indexer->handleWebsite()).' . PHP_EOL;
            }
        });

        // Fetch the XML documents and store the URL's inside them.
        if (count($robotsTxt->xmlSiteMaps) > 0) {
            foreach ($robotsTxt->xmlSiteMaps as $xmlFileUrl) {
                try {
                    $fetchedFile = file_get_contents($xmlFileUrl);
                    $xml = new DOMDocument();
                    $xml->loadXML($fetchedFile);
                    $xml = $xml->getElementsByTagName('loc');

                    foreach ($xml as $item) {
                        $xmlUrlsToScrape->add($item);
                    }
                } catch (Exception $e) {
                    if (IndexerCommand::$verboseLevel > 0) {
                        echo '[WARNING] ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;
                    }
                }
            }
        }

        $this->startCrawlingAndIndexingWebsite($website, $robotsTxt);

        return 0;
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PRIVATE METHODS
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * @param Website $website
     * @param RobotsTxt|null $robotsTxt
     */
    private function startCrawlingAndIndexingWebsite(Website $website, ?RobotsTxt $robotsTxt): void
    {
        // Returned shapes for documents.
        $webpageShapesSplStorage = new SplObjectStorage();

        // Fetch the Default Document and handle it.
        $defaultDocumentResult = null;
        $handleDefaultDocumentDeferred = $this->crawlAndScrapeUrl($website->url);
        $handleDefaultDocumentDeferred->then(function (WebpageShape $webpageShape) use (&$webpageShapesSplStorage) {
            $webpageShapesSplStorage->attach($webpageShape);

            $currentCycle = 0;
            while ($webpageShapesSplStorage->count() > 0) {
                if ($currentCycle > IndexerCommand::$maximumDocumentsAllowedToIndexPerWebsite) {
                    break;
                }

                // Handle all the WebpageShape-objects. Inside the foreach-loop, elements get handled.
                foreach ($webpageShapesSplStorage as $webpageShape) {
                    // Anonymous method to push scraped WebpageShapes.
                    $handleFetchedWebpageShape = function (WebpageShape $webpageShape) use ($webpageShapesSplStorage) {
                        $documentElementsShape = $this->handleScrapedDocumentElements($webpageShape);
                        $webpageShapesSplStorage->attach($documentElementsShape);
                        $this->saveWebpageToDatabase($documentElementsShape);
                    };

                    // Handle URL's!
                    foreach ($webpageShape->urls as $url) {
                        try {
                            $domDocPromise = $this->crawlAndScrapeUrl($url);

                            $domDocPromise->then(function (WebpageShape $webpageShape)
                            use (&$website, &$url, &$handleFetchedWebpageShape) {

                                $webpageShape->websiteId = $website->id;
                                $webpageShape->url = $url;
                                $handleFetchedWebpageShape($webpageShape);
                            });
                        } catch (Exception $e) {
                            if (IndexerCommand::$verboseLevel > 0) {
                                echo '[ERROR] ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;
                            }
                        }
                    }

                    // Handle all anchors!
                    foreach ($webpageShape->anchorElements as $anchorElement) {
                        try {
                            $domDocPromise = $this->crawlAndScrapeUrl($anchorElement->getAttribute('href'));

                            $domDocPromise->then(function (WebpageShape $webpageShape)
                            use (&$anchorElement, &$website, &$handleFetchedWebpageShape) {

                                $webpageShape->websiteId = $website->id;
                                $webpageShape->url = $anchorElement->getAttribute('href');
                                $handleFetchedWebpageShape($webpageShape);
                            });
                        } catch (Exception $e) {
                            if (IndexerCommand::$verboseLevel > 0) {
                                echo '[ERROR] ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;
                            }
                        }
                    }
                }
            }
        });;

    }

    /**
     * Crawls the passed URL.
     * It will return a Promise, asynchronously fetching the HTML-source and returning
     * a WebpageShape as a result.
     *
     * @param string $url
     * @return Promise
     */
    private function crawlAndScrapeUrl(string $url): Promise
    {
        $handleDocumentDeferred = new Deferred();

        try {
            $fetchedDocument = (new HttpGet())->fetchDocument($url);

            if ($fetchedDocument instanceof DOMDocument) {
                $handleDocumentDeferred->resolve($this->handleDocument(null, $fetchedDocument));
            } else {
                throw new Exception('The passed variable isn\'t a DOMDocument.');
            }
        } catch (Exception $e) {
            if (IndexerCommand::$verboseLevel > 1) {
                echo '[WARNING] ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;
            }

            $handleDocumentDeferred->reject();
        }
        $handleDocumentPromise = $handleDocumentDeferred->promise();
        return $handleDocumentPromise;
    }

    /**
     * Responsible for fetched documents. It will scrape all relevant information and take care of it.
     *
     * @param DOMDocument $document
     * @return WebpageShape
     */
    private function handleDocument(?Website $website, DOMDocument $document): WebpageShape
    {
        $webpageShape = new WebpageShape();
        $html = $document->documentElement->textContent;

        // Set META elements.
        $webpageShape->pageTitle = ($document->getElementsByTagName('title'))->item(0)->nodeValue;
        $webpageShape->metaDescription = MetaTags::scrapeMetaDescriptionTag_ToString($document);

        // Scrape the elements.
        $anchorsInHtml = AnchorElement::scrapeElements($document);
        $anchorsInHtml = AnchorElement::processScrapedElements($anchorsInHtml);
        $otherUrlsInHtml = AnchorElement::scrapeUrlsFromHtmlString($html);

        // TODO: scrape images and videos
        $imagesInHtml = new Collection();
        $videosInHtml = new Collection();

        // Push loosely scraped URL's and normal elements to the $documentElementsShape.
        // For every action, unset variables afterwards to save memory.
        try {
            foreach ($anchorsInHtml as $item) {
                $webpageShape->anchorElements->attach($item);
            }

            unset($anchorsInHtml);

            foreach ($otherUrlsInHtml as $url) {
                $webpageShape->urls[] = $url;
            }

            unset($otherUrlsInHtml);

            foreach ($imagesInHtml as $item) {
                $webpageShape->imageElements->attach($item);
            }

            unset($imagesInHtml);

            foreach ($videosInHtml as $item) {
                $webpageShape->videoElements->attach($item);
            }

            unset($videosInHtml);
        } catch (Exception $e) {
            if (IndexerCommand::$verboseLevel > 0) {
                echo '[ERROR] ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;
            }
        }


        return $webpageShape;
    }

    /**
     * This function takes the DocumentsElementsShape and checks if the URL has already been indexed.
     * It also detects new websites. Returns only the necessary information.
     *
     * @param WebpageShape $documentElementsShape
     * @return WebpageShape
     */
    private function handleScrapedDocumentElements(WebpageShape $documentElementsShape): WebpageShape
    {
        /*
         * Prevent indexing documents that have been handled already.
         * */
        try {
            // Foreach all the already handled URL's by the Indexer.
            foreach ($this->indexedUrls as $alreadyIndexedUrl) {
                // Check if the newly scraped AnchorElements has already been processed by the Indexer.
                foreach ($documentElementsShape->anchorElements as $key => $anchorElement) {
                    if ($alreadyIndexedUrl == $anchorElement->getAttribute('href')) {
                        $documentElementsShape->anchorElements->detach($anchorElement);
                    }
                }

                foreach ($documentElementsShape->urls as $key => $url) {
                    if ($alreadyIndexedUrl == $url) {
                        unset($documentElementsShape->urls[$key]);
                    }
                }

                // TODO: images and video URL-handling.
            }

            /* * * * * * * * * * * * * * * * * *
             * Handle new Elements and URL's   *
             * * * * * * * * * * * * * * * * * */

            // Is this a newly discovered website?
            $websiteExistsInDatabase = function (string $url): bool {
                $website = null;

                if (Cache::has('website.' . $url)) {
                    $website = Cache::get('website' . $url);
                } else {
                    $website = $this->websiteRepo->findWhereFirst('url', $url);
                    CacheRepository::putByString('website' . $url, $url);
                }

                if (!is_null($website)) {
                    return true;
                } else {
                    return false;
                }
            };

            /*
             * Check for all URL's if the website it points to has already been indexed, or not.
             * */
            foreach ($documentElementsShape->urls as $key => $url) {
                $url = Helpers::parseUrlToCleanDomainUrl($url);

                if (!$websiteExistsInDatabase($url)) {
                    unset($documentElementsShape->urls[$key]);
                    event(new NewWebsiteDiscoveredEvent($url));
                }
            }

            foreach ($documentElementsShape->anchorElements as $key => $anchorElement) {
                $url = Helpers::parseUrlToCleanDomainUrl($anchorElement->getAttribute('href'));

                if (!$websiteExistsInDatabase($url)) {
                    $documentElementsShape->anchorElements->detach($anchorElement);
                    event(new NewWebsiteDiscoveredEvent($url));
                }
            }

            // TODO: determine the file/document type. Save them to the database.
        } catch (Exception $e) {
            if (IndexerCommand::$verboseLevel > 0) {
                dd($e);
            }
        }

        return $documentElementsShape;
    }

    private function saveWebpageToDatabase(WebpageShape $webpageShape): void
    {
        try {
            dd($webpageShape);
            $webpage = $this->webpageRepo->create([
                'url' => $webpageShape->url,
                'page_title' => $webpageShape->pageTitle,
                'meta_description' => $webpageShape->metaDescription,
                'meta_keywords' => $webpageShape->metaKeywords,
                'meta_author' => $webpageShape->metaAuthor,
                'html_content' => $webpageShape->htmlContent,
                'alternative_description' => 'ALT_DESCRIPTION',
                'last_index' => now(),
                'website_id' => $webpageShape->websiteId,
            ]);

            dd($webpage);
        } catch (Exception $e) {
            dd($e);
        }
    }
}
