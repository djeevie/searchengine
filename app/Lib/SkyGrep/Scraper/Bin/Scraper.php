<?php

namespace App\Lib\SkyGrep\Scraper\Bin;

use App\Events\IndexingWebsiteCompletedEvent;
use App\Lib\Indexer\Bin\Scrapers\AnchorElement;
use App\Lib\SkyGrep\Scraper\Bin\Etc\RobotsTxt;
use App\Models\WebContext\Website;
use App\Repositories\Contracts\IWebsiteRepository;
use Exception;
use Illuminate\Support\Collection;
use React\Promise\Deferred;
use React\Promise\Promise;
use SplObjectStorage;

/**
 * Class Crawler
 * @package App\Lib\WebCrawler\Bin
 */
final class Scraper
{
    /**
     * Holds the instance of this singleton class.
     *
     * @var Scraper
     */
    private static Scraper $instance;

    /**
     * @var IWebsiteRepository
     */
    private IWebsiteRepository $websiteRepo;

    /**
     * @var SplObjectStorage
     */
    private SplObjectStorage $websiteQueue;

    /**
     * @var SplObjectStorage
     */
    private SplObjectStorage $indexWebsiteTasks;



    /**
     * The current working status of the instance.
     *
     * @var bool
     */
    private bool $isOperating = false;

    /*
     * PUBLIC METHODS
     * */

    /**
     * Crawler constructor.
     * @param IWebsiteRepository $websiteRepo
     */
    private function __construct()
    {
        $this->websiteRepo = app()->make(IWebsiteRepository::class);
        $this->websiteQueue = new SplObjectStorage();
        $this->indexWebsiteTasks = new SplObjectStorage();
    }

    /**
     * @return Scraper
     */
    public static function getInstance(): Scraper
    {
        if (empty(self::$instance)) {
            self::$instance = new Scraper;
        }

        return self::$instance;
    }

    /**
     * @param Collection $websites
     * @return void
     */
    public function attachWebsite(Website $website): void
    {
        self::$instance->websiteQueue->attach($website);

        return;
    }
}
