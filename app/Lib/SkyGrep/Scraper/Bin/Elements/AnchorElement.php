<?php

namespace App\Lib\SkyGrep\Scraper\Bin\Elements;

use App\Console\Commands\IndexerCommand;
use App\Models\WebContext\HtmlElementContext\HtmlAnchorElement;
use App\Repositories\Cache\Concrete\CacheRepository;
use DOMDocument;
use DOMElement;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use SebastianBergmann\CodeCoverage\Report\PHP;


/**
 * Class AnchorElement
 * @package App\Lib\SkyGrep\Scraper\Bin\Elements
 */
final class AnchorElement extends BaseElement
{
    /**
     * Specifies which protocols are allowed for anchors.
     *
     * @var array|string[]
     */
    private static array $allowedAnchorProtocols = [
        'http',
        'https',
    ];

    /**
     * Scrape the DOM for elements and return them.
     *
     * @param DOMDocument
     * @return Collection
     */
    public static function scrapeElements(DOMDocument $document): Collection
    {
        $elements = new Collection();

        try {
            foreach ($document->getElementsByTagName('a') as $element) {
                $elements->add($element);
            }
        } catch (Exception $e) {
            echo '[WARNING] ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;
        }

        return $elements;
    }

    /**
     * Process the passed Collection with DOMElements. Returns filtered elements.
     *
     * @param Collection $elements
     * @return int
     */
    public static function processScrapedElements(Collection $elements): Collection
    {
        $filteredAnchorElements = new Collection();

        foreach ($elements as $element) {
            $filteredElement = static::filterElement($element);

            if (is_null($filteredElement)) {
                continue;
            }

            echo 'FILTERED ELEMENT:' . PHP_EOL;
            var_dump($filteredElement);
            $filteredAnchorElements->add($element);
        }

        return $filteredAnchorElements;
    }

    /**
     * Filter an element. Every kind of DOMElement requires a different way of handling them.
     *
     * @param DOMElement $element
     * @return DOMElement|null
     */
    public static function filterElement(DOMElement $element): ?DOMElement
    {
        $href = $element->getAttribute('href');
        $textContent = $element->textContent;

        // Does the URL contain two slashes? Is it without whitespace?
        if (!is_int(strpos($href, '//') || is_int(strpos($href, ' ')))) {
            return null;
        }

        // Filter the HREF-attribute to check the anchor is valid.
        $anchorValid = false;

        foreach (self::$allowedAnchorProtocols as $protocol) {
            if (is_int(strpos($href, $protocol))) {
                $anchorValid = true;
            }
        }

        if (!$anchorValid) {
            return null;
        }

        return $element;
    }

    /**
     * @param string $html
     * @return Collection
     */
    public static function scrapeUrlsFromHtmlString(string $html): Collection
    {
        $scrapedUrls = new Collection();

        foreach (preg_split("/((\r?\n)|(\r\n?))/", $html) as $line) {
            try {
                if (!str_contains($line, 'http')) {
                    continue;
                }

                // Strip whitespace.
                $dirtyUrl = preg_replace('@ @', '', $line);

                // Trim left side of the line
                $url = (function($dirtyUrl) {
                    $httpPosition = strpos($dirtyUrl, 'http');
                    $dirtyUrl = substr($dirtyUrl, $httpPosition, strlen($dirtyUrl));

                    return filter_var($dirtyUrl, FILTER_SANITIZE_URL);
                })($dirtyUrl);

                $scrapedUrls->add($url);

            } catch (Exception $e) {
                if (IndexerCommand::$verboseLevel > 0) {
                    echo '[ERROR] ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;
                }
            }
        }

        return $scrapedUrls;
    }
}
