<?php

namespace App\Lib\SkyGrep\Scraper\Bin\Elements\MetaTags;

use App\Console\Commands\IndexerCommand;
use App\Lib\SkyGrep\Scraper\Bin\Elements\BaseElement;
use DOMDocument;
use DOMNode;
use Illuminate\Support\Collection;

/**
 * Class MetaTags
 * @package App\Lib\SkyGrep\Scraper\Bin\Elements\MetaTags
 */
class MetaTags implements IMetaTags
{

    /**
     * Scrape the DOM for elements and return them.
     *
     * @param DOMDocument
     * @return Collection
     */
    public static function scrapeElements(DOMDocument $document): Collection
    {
        $metaTags = new Collection();

        $elements = $document->getElementsByTagName('meta');

        foreach ($elements as $element) {
            $metaTags->add($element);
        }

        return $metaTags;
    }

    /**
     * @param DOMDocument $document
     * @return string
     */
    public static function scrapeMetaDescriptionTag_ToString(DOMDocument $document): string
    {
        $metaTags = self::scrapeElements($document);

        foreach ($metaTags as $metaTag) {
            try {
                if ($metaTag->attributes[0]->localName == 'description') {
                    return $metaTag->attributes[0]->value;
                } else {
                    return '';
                }

            } catch (\Exception $e) {
                if (IndexerCommand::$verboseLevel > 0) {
                    echo '[ERROR]: ' . $e->getMessage() . ' on line ' . $e->getLine() . PHP_EOL;
                }

                return '';
            }
        }
    }

    /**
     * @param DOMDocument $document
     * @return string|null
     */
    public static function scrapeMetaKeywordsTag_ToString(DOMDocument $document): ?string
    {
        // TODO: Implement scrapeMetaKeywordsTag() method.
    }

    /**
     * @param DOMDocument $document
     * @return string|null
     */
    public static function scrapeMetaAuthorTag_ToString(DOMDocument $document): ?string
    {
        // TODO: Implement scrapeMetaAuthorTag() method.
    }
}
