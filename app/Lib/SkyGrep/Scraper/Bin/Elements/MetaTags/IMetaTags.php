<?php

namespace App\Lib\SkyGrep\Scraper\Bin\Elements\MetaTags;

use DOMDocument;
use DOMNode;
use Illuminate\Support\Collection;

/**
 * Interface IMetaTags
 * @package App\Lib\SkyGrep\Scraper\Bin\Elements\MetaTags
 */
interface IMetaTags
{
    /**
     * @param DOMDocument $document
     * @return Collection
     */
    public static function scrapeElements(DOMDocument $document): Collection;

    /**
     * @param DOMDocument $document
     * @return string|null
     */
    public static function scrapeMetaDescriptionTag_ToString(DOMDocument $document): ?string;

    /**
     * @param DOMDocument $document
     * @return string|null
     */
    public static function scrapeMetaKeywordsTag_ToString(DOMDocument $document): ?string;

    /**
     * @param DOMDocument $document
     * @return string|null
     */
    public static function scrapeMetaAuthorTag_ToString(DOMDocument $document): ?string;
}
