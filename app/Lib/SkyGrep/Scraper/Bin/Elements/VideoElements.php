<?php

namespace App\Lib\Indexer\Bin\Scrapers;

use DOMDocument;
use Illuminate\Support\Collection;

/**
 * Class VideoElements
 * @package App\Lib\WebCrawler\Bin\Elements
 */
final class VideoElements extends BaseElement
{

    /**
     * Scrape the DOM for elements and return them.
     *
     * @return Collection
     */
    protected static function scrapeElements(DOMDocument $document): Collection
    {
        // TODO: Implement scrapeElements() method.
    }
}
