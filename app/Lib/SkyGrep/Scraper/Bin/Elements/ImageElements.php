<?php

namespace App\Lib\Indexer\Bin\Scrapers;

use DOMDocument;
use Illuminate\Support\Collection;

/**
 * Class ImageElements
 * @package App\Lib\WebCrawler\Bin\Elements
 */
final class ImageElements extends BaseElement
{

    /**
     * Scrape the DOM for elements and return them.
     *
     * @return Collection
     */
    protected static function scrapeElements(DOMDocument $document): Collection
    {
        // TODO: Implement scrapeElements() method.
    }

    /**
     * Process the passed DOM-elements.
     *
     * @param Collection $elements
     * @return int
     */
    static public function processElements(Collection $elements): Collection
    {
        // TODO: Implement processElements() method.
    }
}
