<?php

namespace App\Lib\SkyGrep\Scraper\Bin\Elements\Shapes;

use Illuminate\Support\Collection;
use SplObjectStorage;

/**
 * Class WebpageShape
 * @package App\Lib\SkyGrep\Scraper\Bin\Elements\Shapes
 */
final class WebpageShape
{
    /**
     * @var int
     */
    public int $websiteId;

    /**
     * @var string
     */
    public string $url = '';

    /**
     * @var string
     */
    public string $pageTitle = '';

    /**
     * @var string
     */
    public string $metaDescription = '';

    /**
     * @var Collection
     */
    public Collection $metaKeywords;

    /**
     * @var string
     */
    public string $metaAuthor = '';

    /**
     * @var string
     */
    public string $htmlContent = '';

    /**
     * @var array
     */
    public array $urls;

    /**
     * @var SplObjectStorage
     */
    public SplObjectStorage $anchorElements;

    /**
     * @var SplObjectStorage
     */
    public SplObjectStorage $imageElements;

    /**
     * @var SplObjectStorage
     */
    public SplObjectStorage $videoElements;

    /**
     * WebpageShape constructor.
     */
    public function __construct()
    {
        $this->urls = [];
        $this->anchorElements = new SplObjectStorage();
        $this->imageElements = new SplObjectStorage();
        $this->videoElements = new SplObjectStorage();
    }
}
