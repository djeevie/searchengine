<?php

namespace App\Lib\SkyGrep\Scraper\Bin\Elements;

use DOMDocument;
use DOMElement;
use Illuminate\Support\Collection;
use React\Promise\Deferred;
use React\Promise\Promise;

/**
 * Class BaseElement
 * @package App\Lib\WebCrawler\Bin\Elements
 */
abstract class BaseElement
{
    /**
     * The same as scrapeElements(), but async. Returns a Promise.
     *
     * @return Promise
     */
    public static function scrapeElementsAsync(DOMDocument $document): Promise
    {
        $deferred = new Deferred();

        $deferred->resolve(static::scrapeElements($document));

        return $deferred->promise();
    }

    /**
     * Scrape the DOM for elements and return them.
     *
     * @param DOMDocument
     * @return Collection
     */
    abstract public static function scrapeElements(DOMDocument $document): Collection;

    /**
     * Process the passed Collection with DOMElements. Returns filtered elements.
     *
     * @param Collection $elements
     * @return int
     */
    abstract public static function processScrapedElements(Collection $elements): Collection;

    /**
     * Filter an element. Every kind of DOMElement requires a different way of handling them.
     *
     * @param DOMElement $element
     * @return DOMElement|null
     */
    abstract public static function filterElement(DOMElement $element): ?DOMElement;

    /**
     * @param string $html
     * @return Collection
     */
    abstract public static function scrapeUrlsFromHtmlString(string $html): Collection;
}
