<?php

namespace App\Providers;

use App\Models\WebContext\TLD;
use App\Repositories\Cache\Concrete\CacheRepository;
use Illuminate\Support\ServiceProvider;

class CacheServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $tlds = TLD::all();
        CacheRepository::putByString('tlds', $tlds);
    }
}
