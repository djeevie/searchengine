<?php

namespace App\Providers;

use App\Listeners\AddWebpageToDatabase;
use App\Listeners\AddWebsiteToDatabase;
use App\Listeners\CheckAndUpdateIndexerOperatingStatus;
use App\Listeners\SaveWebsiteToCache;
use App\Listeners\UpdateIndexerDocCounters;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        'App\Events\NewWebsiteDiscoveredEvent' => [
            'App\Listeners\AddWebsiteToDatabase',
        ],

        'App\Events\IndexingDocumentCompletedEvent' => [
            'App\Listeners\AddWebpageToDatabase',
        ],

        'App\Events\NewWebpageDiscoveredEvent' => [
            'App\Listeners\AddWebpageToDatabase',
        ],

        // Specific order to keep cache up-to-date.
        'App\Events\IndexingWebsiteCompletedEvent' => [
            'App\Listeners\CheckAndUpdateIndexerOperatingStatus',
            'App\Listeners\UpdateIndexerDocCounters',
            'App\Listeners\SaveWebsiteToCache',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
