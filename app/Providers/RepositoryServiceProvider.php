<?php

namespace App\Providers;

use App\Repositories\Contracts\IUserRepository;
use App\Repositories\Contracts\IWebpageRepository;
use App\Repositories\Contracts\IWebsiteRepository;
use App\Repositories\Eloquent\EloquentUserRepository;
use App\Repositories\Eloquent\EloquentWebpageRepository;
use App\Repositories\Eloquent\EloquentWebsiteRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IUserRepository::class, EloquentUserRepository::class);
        $this->app->bind(IWebsiteRepository::class, EloquentWebsiteRepository::class);
        $this->app->bind(IWebpageRepository::class, EloquentWebpageRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
