<?php

namespace App\Models\WebContext\HtmlElementContext;

use App\Models\WebContext\Webpage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class HtmlVideoElement extends Model
{
    use HasFactory;

    public function webpage(): BelongsTo
    {
        return $this->belongsTo(Webpage::class);
    }
}
