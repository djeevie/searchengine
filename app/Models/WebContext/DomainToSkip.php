<?php

namespace App\Models\WebContext;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DomainToSkip extends Model
{
    use HasFactory;

    protected $table = 'domains_to_skip';
}
