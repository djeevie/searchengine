<?php

namespace App\Models\WebContext;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Website extends Model
{
    use HasFactory;

    public object $robotsTxt;

    public function tld(): BelongsTo
    {
        return $this->belongsTo(TLD::class);
    }

    public function webpages(): HasMany
    {
        return $this->hasMany(Webpage::class);
    }
}
