<?php

namespace App\Models\WebContext;

use App\Lib\SkyGrep\Scraper\Bin\Elements\Shapes\WebpageShape;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Webpage extends Model
{
    use HasFactory;

    public function website(): BelongsTo
    {
        return $this->belongsTo(Website::class);
    }
}
