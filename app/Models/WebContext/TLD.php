<?php

namespace App\Models\WebContext;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TLD extends Model
{
    use HasFactory;

    protected $table = 'domain_tlds';

    public $timestamps = false;

    public function domains(): HasMany
    {
        return $this->hasMany(Domain::class);
    }
}
