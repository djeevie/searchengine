<h1 align="center"><a href="https://skygrep.com" target="_blank">openSkyGrep</a></h1>
The Open-Source Decentralized Search Engine

# About openSkyGrep
openSkyGrap is our open-source edition of SkyGrep.com. The application scrapes and indexes the internet, and shows the 
most relevant webpages to the user.

You can run this application on your own servers and even
create federation relationships with servers hosted by others! This saves bandwidth, computing power and assures 
censorship by governments is impossible.

# Installation
<b>Step 1</b>:
Clone the repository. You will be checked out in the master-branch by default.

~$ git clone https://gitlab.com/djeevie/searchengine.git

<b>Step 2:</b>
Copy the .env.example in the root directory to .env:

~$ cp .env.example .env

<b>Step 3:</b>
Open and edit the new .env file to configure the application according to your needs.

~$ nano .env 
(or open with a GUI)

<b>Step 4:</b>
Install the dependencies via Composer:

~$ composer install

<b>Step 5:</b>
Run the migrations and seeders, to set up your database:

~$ php artisan migrate --seed

<b>Step 6:</b>
Run the application!

Now it's time to run your freshly installed application. Just run this command to start scraping the web:

~$ php artisan crawler:run

# Contribute
Contributions to the platform are very welcome.

Are you an experienced Laravel developer with some spare time? Please consider contributing, so we can 
make the world a better place by decentralizing search engines.

# License
This open-source project has been distributed under the Apache 2.0 License. Please read LICENSE.txt and make
sure you comply with the license.
